# Laptop Service

from flask import Flask
from flask_restful import Resource, Api

# Instantiate the app
app = Flask(__name__)
api = Api(app)



class Yeet(Resource):
    def get(self, id=None):
        app.logger.debug("Got a JSON request")
        return {
            'Laptops': ['mmS', 'f', 
            'gt',
	    'Yet sgbd laptop!',
	    'Yet fsb another laptop!'
            ]
        }
    

# Create routes
# Another way, without decorators
api.add_resource(Yeet, '/<id>')
app.logger.debug("goingt")


class Laptop(Resource):
    def get(self):
        app.logger.debug(" hello request")
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }

api.add_resource(Laptop, '/hi')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
