"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
#CONFIG = config.configuration()
#app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    _items = db.tododb.find()
    
    items = [item for item in _items]
    #app.logger.debug(items + "HELLO")
    return flask.render_template('calc.html', items=items)


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    
    open_time = acp_times.open_time(km, 200, '2013-05-05')
    close_time = acp_times.close_time(km, 200, '2013-05-05')
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)
'''
@app.route('/new', methods=['POST'])
def new():
    item_doc = {
        'km': request.form['km'],
        'distance': request.form['distance']
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('_calc_times'))
'''
@app.route('/new', methods=['POST'])
def new():
    for x in range(len(request.form.getlist('km'))):
        if request.form.getlist('km')[x] != "":
            item_doc = {
                'km': request.form.getlist('km')[x],
                'distance': request.form['distance'],
                'open': request.form.getlist('open')[x],
                'close': request.form.getlist('close')[x]
            }
            db.tododb.insert_one(item_doc)

    return redirect(url_for('index'))

@app.route('/newer')
def trial():
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('displayer.html', items=items)



class Listall(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        return items

api.add_resource(Listall, '/listAll')
api.init_app(app)
#############
'''
app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)
'''
if __name__ == "__main__":
    #print("Opening for global access on port {}".format(CONFIG.PORT))
    #app.run(port=CONFIG.PORT, host="0.0.0.0")
    app.run(host='0.0.0.0', debug=True)
